package com.enlighten.puzzle;

import com.enlighten.puzzle.exceptions.PuzzleFormatException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class InputBeanTest {

    @Test
    public void testBean() throws PuzzleFormatException {

        InputBean input = new InputBean(3,3);
        List<String> gridData = new ArrayList<>();
        gridData.add("A B C");
        gridData.add("D E F");
        gridData.add("G H I");
        input.setGrid(gridData);

        char[][] puzzleData = input.getGridData();
        char[][] expectedData = {
                {'A','B','C'},
                {'D','E','F'},
                {'G','H','I'},
        };
        Assertions.assertArrayEquals(expectedData, puzzleData);

        List<String> answersForKey = new ArrayList<>();
        answersForKey.add("ABC");
        answersForKey.add("AEI");
        input.setHiddenWordValues(answersForKey);

        String[] expected = {"ABC","AEI"};
        Assertions.assertArrayEquals(expected, input.getHiddenWordValues());



    }

    @Test
    public void testInvalidDataSize() throws PuzzleFormatException {
        try {
            new InputBean(0,3);
            Assertions.fail("constructor with 0 or < zero.");
        } catch(PuzzleFormatException e ) {/* PuzzleFormatException when creating with negative or zero size */ }
        try {
            new InputBean(3,0);
            Assertions.fail("constructor with 0 or < zero.");
        } catch(PuzzleFormatException e ) { /* PuzzleFormatException when creating with negative or zero size */ }
        try {
            new InputBean(0,0);
            Assertions.fail("constructor with 0 or < zero.");
        } catch(PuzzleFormatException e ) { /* PuzzleFormatException when creating with negative or zero size */ }
        try {
            new InputBean(3,-1);
            Assertions.fail("constructor with 0 or < zero.");
        } catch(PuzzleFormatException e ) {/* PuzzleFormatException when creating with negative or zero size */ }

        InputBean input = new InputBean(3, 3);
        try {
            input.setGrid(null);
            Assertions.fail("setGrid - null");
        } catch (PuzzleFormatException e) {/* PuzzleFormatException when setGrid is null */ }
        try {
            input.setGrid(new ArrayList<>());
            Assertions.fail("setGrid - no row or empty");
        } catch (PuzzleFormatException e) {/* PuzzleFormatException when setGrid is null */ }
        try {
            List<String> l = new ArrayList<>();
            l.add("123");
            l.add("1235");
            l.add("321");
            input.setGrid(l);
            Assertions.fail("setGrid - column too big");
        } catch (PuzzleFormatException e) {/* PuzzleFormatException when setGrid is null */ }
        try {
            List<String> l = new ArrayList<>();
            l.add("123");
            l.add("321");
            l.add("12");
            input.setGrid(l);
            Assertions.fail("setGrid - column too small");
        } catch (PuzzleFormatException e) {/* PuzzleFormatException when setGrid is null */ }
    }

    @Test
    public void testInvalidAnswerSize() throws PuzzleFormatException {
        InputBean input = new InputBean(3, 3);
        List<String> gridData = new ArrayList<>();
        gridData.add("A B C");
        gridData.add("D E F");
        gridData.add("G H I");
        input.setGrid(gridData);
        try {
            input.setHiddenWordValues(null);
            Assertions.fail("setAnswerValues - null");
        } catch (PuzzleFormatException e) { /* Should throw exception */ }

        try {
            List<String> answersForKey = new ArrayList<>();
            answersForKey.add("ABCD");
            answersForKey.add("AEI");
            input.setHiddenWordValues(answersForKey);
            Assertions.fail("setAnswerValues - too large");
        } catch (PuzzleFormatException e) { /* Should throw exception */ }

    }
}
