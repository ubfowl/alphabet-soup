package com.enlighten.puzzle.wordsearch;

import com.enlighten.puzzle.WordPuzzleI;
import com.enlighten.puzzle.exceptions.PuzzleException;
import com.enlighten.puzzle.exceptions.PuzzleFormatException;
import com.enlighten.puzzle.exceptions.WordNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class WordSearchTest {
    @Test
    public void testAnswerKey() throws PuzzleException {
        WordSearch puzzle = new WordSearch(case5By5());
        String[] words = {
                "HELLO",
                "GOOD",
                "BYE",
        };
        String[] results = puzzle.answerKey(words);
        String[] expected = {
                "HELLO 0:0 4:4",
                "GOOD 4:0 4:3",
                "BYE 1:3 1:1",
        };
        Assertions.assertArrayEquals(expected, results);


        words = new String[]  {
                "HELFF",
                "GOOD",
                "BYE",
        };
        results = puzzle.answerKey(words);
        expected = new String[] {
                "ERROR: Word 'HELFF' not found in puzzle",
                "GOOD 4:0 4:3",
                "BYE 1:3 1:1",
        };
        Assertions.assertArrayEquals(expected, results);

        puzzle.answerKey(null);
    }
    @Test
    public void testConstructionWordSearch() {
        // ---- Testing with one row
        try {
            Assertions.assertInstanceOf(WordPuzzleI.class, new WordSearch(new char[][]{ { 'a' } }));
        } catch (PuzzleFormatException e) {
            Assertions.fail("Should not reach here when all lengths are the same");
        }

        // ---- Testing with multiple rows
        try {
            new WordSearch(new char[][]{ {'a', 'b', 'c'}, {'1', '2', '3'}, {'a', 'b', 'c'}, });
        } catch (PuzzleFormatException e) {
            Assertions.fail("Should not reach here when all lengths are the same");
        }

        // ---- Testing exception with empty data
        try {
            new WordSearch(new char[0][0]);
            Assertions.fail("Should not reach here when any element is empty array  - exception should be thrown");
        } catch (PuzzleFormatException e) {
            // testing for the excpetion
        }

        // Testing exception with empty data
        try {
            new WordSearch(null);
            Assertions.fail("Should not reach here when puzzle data null  - exception should be thrown");
        } catch (PuzzleFormatException e) { /*Exception handling is working */ }

        // ---- Testing exception with a row being null
        try {
            // Not first row handling
            new WordSearch(new char[][]{ {'a', 'b', 'c'}, {'a', 'b', 'c'}, null, });
            Assertions.fail("Should not reach here when any element is null  - exception should be thrown");

            // First row handling
            new WordSearch(new char[][]{ null, {'a', 'b', 'c'}, {'a', 'b', 'c'}, });
            Assertions.fail("Should not reach here when any element is null  - exception should be thrown");
        } catch (PuzzleFormatException e) { /* Exception handling is working */ }

        // ---- Testing exception with not all rows same length
        try {
            new WordSearch(new char[][]{ {'a', 'b', 'c'}, {'1', '3'}, {'a', 'b', 'c'}, });
            Assertions.fail("Should not reach here when all lengths are not the same - exception should be thrown");
        } catch (PuzzleFormatException e) { /* Exception handling is working */ }
    }

    @Test
    public void testCaseInsensitive() throws Exception {
        WordSearch puzzle = new WordSearch(case5By5MixedCase(), false);
        Assertions.assertEquals("Hello 0:0 4:4", (puzzle.search("Hello")).formatAsString());
        Assertions.assertEquals("hell o 0:0 4:4", (puzzle.search("hell o")).formatAsString());
    }

    @Test
    public void testDealWithSpacesInWordToFind() throws Exception {
        // Requirement: Words that have spaces in them will *not* include spaces when hidden in the grid of characters.
        // Within the grid of characters, the words may appear diagonal - forwards
        Assertions.assertEquals("HELL O 0:0 4:4", ((new WordSearch(case5By5())).search("HELL O")).formatAsString());
    }

    @Test
    public void testFindWord() throws Exception {
        // Requirements: Within the grid of characters, the words may appear vertical, horizontal or diagonal.
        //               Within the grid of characters, the words may appear forwards or backwards.

        WordSearch puzzle = new WordSearch(case5By5());
        // Validation tests in the method
        testForBadWordInFind(puzzle);

        // Within the grid of characters, the words may appear horizontal - forwards
        Assertions.assertEquals("GOOD 4:0 4:3", (puzzle.search("GOOD")).formatAsString());

        // Within the grid of characters, the words may appear horizontal - backwards
        Assertions.assertEquals("BYE 1:3 1:1", (puzzle.search("BYE")).formatAsString());

        // Within the grid of characters, the words may appear vertical - backwards
        Assertions.assertEquals("XNO 2:4 4:4", (puzzle.search("XNO")).formatAsString());

        // Within the grid of characters, the words may appear diagonal - backwards
        Assertions.assertEquals("BLY 3:2 1:2", (puzzle.search("BLY")).formatAsString());

        // Within the grid of characters, the words may appear diagonal - forwards
        Assertions.assertEquals("HELLO 0:0 4:4", (puzzle.search("HELLO")).formatAsString());

        // Within the grid of characters, the words may appear vertical - forwards
        Assertions.assertEquals("OLLEH 4:4 0:0", (puzzle.search("OLLEH")).formatAsString());

        // Word not in test puzzle
        try {
            puzzle.search("FRED");
            Assertions.fail("Should not get here - should throw WordNotFoundException");
        } catch (WordNotFoundException e) {
            Assertions.assertEquals("Word 'FRED' not found in puzzle", e.getMessage());
        }

        try {
            puzzle.search("NOA");
            Assertions.fail("Should not get here - should throw WordNotFoundException");
        } catch (WordNotFoundException e) {
            Assertions.assertEquals("Word 'NOA' not found in puzzle", e.getMessage());
        }

        try {
            puzzle.search("FREDERICK");
            Assertions.fail("Should not get here - should throw WordNotFoundException");
        } catch (WordNotFoundException e) {
            Assertions.assertEquals("Puzzle not large enough to have word 'FREDERICK'", e.getMessage());
        }
    }

    private static void testForBadWordInFind(WordSearch puzzle) throws PuzzleException {
        try {
            puzzle.search(null);  // Word longer then and row or column
            Assertions.fail("Should not get here - should throw PuzzleException");
        } catch (PuzzleException e) {
            Assertions.assertEquals("Missing or empty word for find() to search for.", e.getMessage());
        }
        try {
            puzzle.search("");  // Word longer then and row or column
            Assertions.fail("Should not get here - should throw WordNotFoundException");
        } catch (PuzzleException e) {
            Assertions.assertEquals("Missing or empty word for find() to search for.", e.getMessage());
        }
        try {
            puzzle.search("NOT IN PUZZLE");  // Word longer then and row or column
            Assertions.fail("Should not get here - should throw WordNotFoundException");
        } catch (WordNotFoundException e) {
            Assertions.assertEquals("Puzzle not large enough to have word 'NOTINPUZZLE'", e.getMessage());
        }
    }

    private char[][] case5By5() {
        return new char[][]
                {
                        "HASDF".toCharArray(),
                        "GEYBH".toCharArray(),
                        "JKLZX".toCharArray(),
                        "CVBLN".toCharArray(),
                        "GOODO".toCharArray(),

                };
    }
    private char[][] case5By5MixedCase() {
        return new char[][]
                {
                        "HASDF".toLowerCase().toCharArray(),
                        "GEYBH".toCharArray(),
                        "JKLZX".toLowerCase().toCharArray(),
                        "CVBLN".toCharArray(),
                        "GOODO".toLowerCase().toCharArray(),

                };
    }
}
