package com.enlighten.puzzle.wordsearch;

import com.enlighten.puzzle.LocationI;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class WordSearchLocationTest {
    @Test
    public void testToString() {
        WordSearchLocation location = new WordSearchLocation("TEST", 0, 0, 4, 4);
        Assertions.assertInstanceOf(LocationI.class, location);
        Assertions.assertEquals("TEST 0:0 4:4",location.formatAsString());
        Assertions.assertEquals("TEST 0:0 4:4",location.toString());
    }
}
