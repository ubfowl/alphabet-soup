package com.enlighten.puzzle.input;

import com.enlighten.puzzle.InputBean;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class InputFileReaderTest {

    @Test
    public void loadFile() throws Exception {
        InputFileReader fileReader = new InputFileReader("./src/test/testInput.txt");
        InputBean bean = fileReader.buildInputBean();
        Assertions.assertEquals(3, bean.getRowCount());
        Assertions.assertEquals(4, bean.getColumnCount());

        char[][] expected = {
            {'A','B','C','1'},
            {'D','E','F','2'},
            {'G','H','I','3'},
        };
        Assertions.assertArrayEquals(expected, bean.getGridData());

        String[] exceptedAns = {
                "ABC",
                "AEI",
        };
        Assertions.assertArrayEquals(exceptedAns, bean.getHiddenWordValues());
    }
}
