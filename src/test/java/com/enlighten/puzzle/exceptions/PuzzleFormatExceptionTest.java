package com.enlighten.puzzle.exceptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PuzzleFormatExceptionTest {
    @Test
    public void testPuzzleFormatExceptionException() {
        PuzzleFormatException exception = new PuzzleFormatException("my message");
        Assertions.assertInstanceOf(PuzzleException.class, exception);
        Assertions.assertEquals("my message", exception.getMessage());
    }
}
