package com.enlighten.puzzle.exceptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PuzzleExceptionTest {
    @Test
    public void testPuzzleExceptionException() {
        PuzzleException exception = new PuzzleException("my message");
        Assertions.assertInstanceOf(Exception.class, exception);
        Assertions.assertEquals("my message", exception.getMessage());
    }
}
