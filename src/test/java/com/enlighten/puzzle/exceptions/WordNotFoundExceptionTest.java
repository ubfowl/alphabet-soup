package com.enlighten.puzzle.exceptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class WordNotFoundExceptionTest {
    @Test
    public void testNewWordNotFoundException() {
        WordNotFoundException exception = new WordNotFoundException("my message");
        Assertions.assertInstanceOf(PuzzleException.class, exception);
        Assertions.assertEquals("my message", exception.getMessage());
    }
}
