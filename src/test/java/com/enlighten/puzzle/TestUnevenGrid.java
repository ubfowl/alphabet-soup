package com.enlighten.puzzle;

import com.enlighten.puzzle.input.InputFileReader;
import com.enlighten.puzzle.input.InputReader;
import com.enlighten.puzzle.wordsearch.WordSearch;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * At one point a had a bug with diagonal when the grid rows and columns where not equal.
 * This test was to make sure I cover testing the scenario.  The bug was comparing row index
 * to a column length (maybe the other way around - it was a copy/paste that did not  get
 * caught originally since the test I wrote had column and row length the same)
 */
public class TestUnevenGrid {
    @Test
    public void testUnevenGrid() throws Exception {
        InputReader fileLoader = new InputFileReader("./src/test/unevenGrid.txt");
        InputBean loadedBean = fileLoader.buildInputBean();
        WordSearch wordSearch = new WordSearch(loadedBean.getGridData());
        String[] results = wordSearch.answerKey(loadedBean.getHiddenWordValues());
        String[] expected = {
                "HELLO 0:2 4:6",
                "GOOD 4:2 4:5",
                "BYE 1:5 1:3",
        };
        Assertions.assertArrayEquals(expected,results);
    }
}
