import com.enlighten.puzzle.InputBean;
import com.enlighten.puzzle.input.InputFileReader;
import com.enlighten.puzzle.input.InputReader;
import com.enlighten.puzzle.wordsearch.WordSearch;

public class MainRunner {

    /**
     * Runs the tests using input file "./inputs/sampleInput.txt"
     * which can be overridden with a different file by including
     * a command line argument
     * @param args input file name (optional - default  "./inputs/sampleInput.txt")
     * @throws Exception if and error occurs
     */
    public static void main(String[] args) throws Exception {
        String filename = "./inputs/sampleInput.txt";
        if (args.length > 0)
            filename = args[0];

        InputReader fileLoader = new InputFileReader(filename);
        InputBean loadedBean = fileLoader.buildInputBean();
        WordSearch wordSearch = new WordSearch(loadedBean.getGridData());
        String[] results = wordSearch.answerKey(loadedBean.getHiddenWordValues());
        for (String line: results) {
            System.out.println(line);
        }
    }
}
