package com.enlighten.puzzle.exceptions;

/**
 * Exceptions for when and attempt to find a word fails to locate the word in the puzzle data
 */
public class WordNotFoundException extends PuzzleException {

    public WordNotFoundException(String message) {
        super(message);
    }
}
