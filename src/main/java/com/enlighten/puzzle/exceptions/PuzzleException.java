package com.enlighten.puzzle.exceptions;

/**
 * Generic exception for handled exceptions in the puzzle system
 */
public class PuzzleException extends Exception {
    public PuzzleException(String message) {
        super(message);
    }
}
