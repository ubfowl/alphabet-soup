package com.enlighten.puzzle.exceptions;

/**
 * Exceptions for when the data placed in the puzzle in formatted incorrectly.
 * This could be irregular size or not conforming to the sizes allocated
 */
public class PuzzleFormatException extends PuzzleException {
    public PuzzleFormatException(String message) {
        super(message);
    }
}
