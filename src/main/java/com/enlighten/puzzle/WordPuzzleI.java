package com.enlighten.puzzle;

import com.enlighten.puzzle.exceptions.PuzzleException;

/**
 * The idea on have WordPuzzleI is that it might be desired to have
 * different types of Puzzles.  For example, we are creating a WordSearch
 * puzzle, and it might be desired to have CrossWord puzzle later
 */
public interface WordPuzzleI {

    /**
     * Searches for the words and returns the answer key
     * @param inputWords words to build the answer key for
     * @return an array of Strings that represents the answer key
     * with one element per input.  If errors like word not found
     * then the string starts with "ERROR: " followed by a description
     * of the error.
     */
    String[]  answerKey(String[] inputWords);

    /**
     * Searches for the word and returns the Location
     * @param wordToFind the word that is to be searched for
     * @return The location
     * @throws PuzzleException in the event the word is not found (WordNotFoundException) within the puzzle or
     * PuzzleException if the wordToFind is invalid like empty, null, or word is larger than puzzle could have
     */
    LocationI search(String wordToFind) throws PuzzleException;
}
