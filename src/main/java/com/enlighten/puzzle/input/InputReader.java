package com.enlighten.puzzle.input;

import com.enlighten.puzzle.InputBean;

/**
 * Interface - implementation needs to know how to build the InputBean.
 * Could be loaded from file (implemented - InputFileReader),
 * user input (Not Yet Implement), database (Not Yet Implement)
 */
public interface InputReader {
    /**
     * loads/creates the InputBean from source
     * @return InputBean ensuplsating the input values for the process.
     * @throws Exception if error occurs in building the input bean
     */
    InputBean buildInputBean() throws Exception;
}
