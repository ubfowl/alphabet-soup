package com.enlighten.puzzle.input;

import com.enlighten.puzzle.InputBean;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is resposible for loading a input file in the format laid out in the challenge
 * and create and input bean to used when creating the WordSearch and then building the
 * answer key output.
 */
public class InputFileReader implements InputReader {
    private final String filename;

    /**
     * @param filename the name of the file that is to be load to
     *                 build the input bean
     */
    public InputFileReader(String filename) {
        this.filename = filename;
    }

    /**
     * reads the input file and populates the InputBean that encapsulates the
     * input data like grid dimensions, grid data, and words hidden within the grid
     */
    @Override
    public InputBean buildInputBean() throws Exception {
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line = reader.readLine();

            // index 0is rows; index 1 = columns
            int[] dims = parseDimension(line);
            InputBean bean = new InputBean(dims[0], dims[1]);

            // load grid row data - might want to break into on method, but not incline so I don't need to
            // pass the reader around
            {
                List<String> gridData = new ArrayList<>();
                for (int i = 0; i < dims[0]; i++) {
                    gridData.add(reader.readLine());
                }
                bean.setGrid(gridData);
            }

            // load words for answer key - might want to break into on method, but not incline so I don't need to
            // pass the reader around
            {
                List<String> answers = new ArrayList<>();
                line = reader.readLine();
                while (line != null) {
                    answers.add(line);
                    line = reader.readLine();
                }
                bean.setHiddenWordValues(answers);
            }
            return bean;
        }
    }

    /**
     * Splits the string the represents the dimensions and
     * returns an array with the row and column counts
     * @param line the string the represents the dimensions (formatted "3x3")
     * @return array with row count at index 0 and column count at index 1
     */
    private int[] parseDimension(String line) {
        String[] split = line.split("x");
        int[] dims = new int[split.length];
        dims[0] = Integer.parseInt(split[0]);
        dims[1] = Integer.parseInt(split[1]);
        return dims;
    }
}
