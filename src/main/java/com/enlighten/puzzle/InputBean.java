package com.enlighten.puzzle;

import com.enlighten.puzzle.exceptions.PuzzleFormatException;

import java.util.ArrayList;
import java.util.List;

/**
 *  The InputBean that encapsulates the input data like
 *  grid dimensions, grid data, and words hidden within the grid
 */
public class InputBean {
    private final char[][] puzzleGrid;
    private final int rowCount;
    private final int columnCount;

    private final List<String> answersForKey;

    /**
     * Constructor
     * @param rowCount number of rows in the word search puzzle
     * @param columnCount number of columns in the word search puzzle
     * @throws PuzzleFormatException when row or column are less than 1
     */
    public InputBean(int rowCount, int columnCount) throws PuzzleFormatException {
        if ((rowCount<=0) || (columnCount<=0)) {
            throw new PuzzleFormatException("Invalid dimensions for data grid");
        }
        this.puzzleGrid = new char[rowCount][columnCount];
        this.rowCount = rowCount;
        this.columnCount = columnCount;
        this.answersForKey = new ArrayList<>();
    }

    /**
     * @return number of rows in the grid
     */
    public int getRowCount() {
        return rowCount;
    }

    /**
     * @return number of columns in the grid
     */
    public int getColumnCount() {
        return columnCount;
    }

    /**
     * Set the puzzles grid data
     * @param gridData a list (ordered) of strings with each string being a representation
     *                 of the row in the grid.
     * @throws PuzzleFormatException if the data is empty; null;or contains values that are not
     * the size for each row.
     */
    public void setGrid(List<String> gridData) throws PuzzleFormatException {
        if ((gridData == null) || (gridData.size() != rowCount))
            throw new PuzzleFormatException("Grid data does not match the dimensions of the InputBean");
        int i = 0;
        for (String s: gridData) {
            s = s.replaceAll(" ", "");
            if (s.length() != columnCount) {
                throw new PuzzleFormatException("Grid data does not match the dimensions of the InputBean");
            }
            puzzleGrid[i++] = s.toCharArray();
        }
    }

    /**
     * @return char[][] that represents the strings set on setGrid
     */
    public char[][] getGridData() {
        return puzzleGrid;
    }

    /**
     *
     * @param hiddenWords a list (ordered) of strings with each word hidden in the grid
     *                that is being requested to be included in the answer key
     * @throws PuzzleFormatException if hiddenWords is empty or null
     */
    public void setHiddenWordValues(List<String> hiddenWords) throws PuzzleFormatException {
        if ((hiddenWords == null) || (hiddenWords.isEmpty()))
            throw new PuzzleFormatException("No answers provided for searching");
        for (String s: hiddenWords) {
            if (s.replaceAll(" ", "").length() > columnCount)
                throw new PuzzleFormatException("Word too large to fit in the column");
            answersForKey.add(s);
        }
    }

    /**
     * @return String[] with the hidden word that are to be used to build the answer key
     */
    public String[] getHiddenWordValues() {
        return answersForKey.toArray(new String[0]);
    }
}
