package com.enlighten.puzzle;

/**
 *  interface to format the string for the location to be used outputting the answer key.
 *  NOTE: Only one implementation not really needed for this exercise, but I could think
 *  in the design it's a good idea to have an abstraction to be used so that if we have
 *  a different time of puzzle other than WordSearch we could have a different format for
 *  something like a CrossWord answer key.
 */
public interface LocationI {
    String formatAsString();
}
