package com.enlighten.puzzle.wordsearch;

import com.enlighten.puzzle.LocationI;

/**
 * hold the location information about the hidden word in the WordSearch puzzle.
 */
public class WordSearchLocation implements LocationI {
    private final int[] fromLocation;
    private final int[] toLocation;

    private final String word;

    /**
     * Consrtructor
     * @param word - hidden word
     * @param fromColumn - index of column for from
     * @param fromRow - index of row for from
     * @param toColumn - index of colum for to
     * @param toRow - index of row for to
     */
    public WordSearchLocation(String word, int fromColumn, int fromRow, int toColumn, int toRow) {
        this.word = word;
        fromLocation = new int[] { fromColumn, fromRow} ;
        toLocation = new int[]  { toColumn, toRow} ;
    }

    /**
     * used for convenience in system.out.print
     * @return same as formatAsString
     */
    @Override
    public String toString() {
        return formatAsString();
    }

    /**
     * formatted string to print for answer key of the words locations.
     * @return formatted string to print for answer key of the words locations.
     */
    public String formatAsString() {
        return word + " " + fromLocation[0]+":"+fromLocation[1]+" "+toLocation[0]+":"+toLocation[1];
    }

}
