package com.enlighten.puzzle.wordsearch;

import com.enlighten.puzzle.LocationI;
import com.enlighten.puzzle.WordPuzzleI;
import com.enlighten.puzzle.exceptions.PuzzleException;
import com.enlighten.puzzle.exceptions.PuzzleFormatException;
import com.enlighten.puzzle.exceptions.WordNotFoundException;

public class WordSearch implements WordPuzzleI {
    /*
    * puzzle is final to ensure nothing accidentally changes the puzzle after creation.
    * Additionally, it is private with no setter methods to encapsulate the data with the
    * instance of the object.  These are done to ensure it is immutable (and data
    * encapsulation principles are applied)
    */
    private final char[][] puzzle;
    private final boolean caseSensitive;

    private final int rows;
    private final int columns;

    private final int maxLengthOfWord;

    /**
     * Constructor creates the WordSearch case-insensitive
     *
     * @param puzzle the char[][] represent the grid for letters
     *               for the word search
     */
    public WordSearch(char[][] puzzle) throws PuzzleFormatException {
        this(puzzle,true);
    }


    /**
     * Constructor creates the WordSearch case-insensitive
     *
     * @param puzzle characters of puzzle
     * @param caseSensitive true = the chars must match the values extactly;
     *                      false = the chars are not case-sensitive and the
     *                      input puzzle will be converted to lower case
     */
    public WordSearch(char[][] puzzle, boolean caseSensitive) throws PuzzleFormatException {
        if ((puzzle == null) || (puzzle.length == 0))
            throw new PuzzleFormatException("Puzzle data is empty");


        int expectedRowLength = (puzzle[0] != null) ? puzzle[0].length:  0;
        for (char[] row: puzzle) {
            if ((row == null) || (row.length == 0) || (row.length != expectedRowLength) )
                throw new PuzzleFormatException("One or more rows in the puzzle data is empty or not same length");
        }

        if (!caseSensitive) {
            for (int i=0; i<puzzle.length; i++) {
                for (int j=0; j<puzzle[i].length; j++) {
                    puzzle[i][j] = Character.toUpperCase(puzzle[i][j]);
                }
            }
        }
        this.puzzle = puzzle;
        this.caseSensitive = caseSensitive;
        this.columns  = puzzle[0].length;
        this.rows = puzzle.length;

        maxLengthOfWord = Math.max(columns, rows);

    }

    /**
     * Searches for the words and returns the string[] fpr the output of the answer key
     *
     * @param inputWords words to build the answer key for
     * @return an array of Strings that represents the answer key
     * with one element per input.  If errors like word not found
     * then the string starts with "ERROR: " followed by a description
     * of the error.  May choose to build DTO type bean for return
     * type
     */
    @Override
    public String[] answerKey(String[] inputWords) {
        if (inputWords == null)
            return null;
        String[] returnInfo = new String[inputWords.length];
        for (int i=0; i<inputWords.length; i++) {
            try {
                LocationI found = search(inputWords[i]);
                returnInfo[i] = found.formatAsString();
            } catch (PuzzleException e) {
                returnInfo[i] = "ERROR: " + e.getMessage();
            }
        }
        return returnInfo;
    }


    /**
     * searches for the location of the word
     *
     * @param wordToFind word to search for.
     * @return The location that word was found - if the word exists more than once only first location encountered is return
     * @throws PuzzleException if input not valid (or WordNotFoundException - when word not found)
     */
    @Override
    public LocationI search(String wordToFind) throws PuzzleException {
        String input = wordToFind;
        if ((wordToFind == null) || (wordToFind.isEmpty()))
            throw new PuzzleException("Missing or empty word for find() to search for.");
        wordToFind = wordToFind.replaceAll(" ", "");
        if (wordToFind.length() > this.maxLengthOfWord)
            throw new WordNotFoundException("Puzzle not large enough to have word '" + wordToFind + "'");
        if (!caseSensitive)
            wordToFind = wordToFind.toUpperCase();

        char[] forward = wordToFind.toCharArray();
        char[] reverse = new char[forward.length];
        int rIdx = forward.length-1;
        for (char c : forward) {
            reverse[rIdx--] = c;
        }
        for (int rowIndex=0; rowIndex<puzzle.length; rowIndex++) {
            for (int columnIndex=0; columnIndex<puzzle[rowIndex].length; columnIndex++) {
                int[] indexes  = testForCell(rowIndex, columnIndex, forward, reverse);
                if (indexes != null)
                    return new WordSearchLocation(input, indexes[0], indexes[1], indexes[2], indexes[3]);
            }
        }

        throw new WordNotFoundException("Word '" + wordToFind + "' not found in puzzle");
    }

    /**
     * Tests for a match at a row and column location
     * @param rowIndex index of row of the character
     * @param columnIndex index of column of the character
     * @param forward char array of the word to test for matching
     * @param reverse reversed char array of the word to test for matching - reverse done once and passed in
     *                to prevent reversing order in each cell;
     * @return null if not matched; int at index:
     *   0 = row index of the first letter;
     *   1 = column index of the first letter;
     *   2 = row index of the last letter;
     *   3 = column index of the last letter;
     */
    private int[] testForCell(int rowIndex, int columnIndex, char[] forward, char[] reverse) {
        int[] ret = horizontal(rowIndex, columnIndex, forward, reverse);
        if (ret == null)
            ret = vertical(rowIndex, columnIndex, forward, reverse);
        if (ret == null)
            ret = diagonal(rowIndex, columnIndex, forward, reverse);
        return ret;
    }

    /**
     * Searches horizontally (both forwards and backwards
     * @param rowIndex index of row of the character
     * @param columnIndex index of column of the character
     * @param forward char array of the word to test for matching
     * @param reverse reversed char array of the word to test for matching - reverse done once and passed in
     *                to prevent reversing order in each cell;
     * @return null if not matched; int at index:
     *   0 = row index of the first letter;
     *   1 = column index of the first letter;
     *   2 = row index of the last letter;
     *   3 = column index of the last letter;
     */
    private int[] horizontal(int rowIndex, int columnIndex, char[] forward, char[] reverse) {
        int endPointer = columnIndex + (forward.length - 1);
        if (endPointer >= columns) // Not Enough chars remain in row to match word
            return null;
        int idxOfChars = 0;
        boolean fwdStillMatching = true;
        boolean reverseStillMatching = true;
        for (int i = columnIndex; i < endPointer; i++) {
            char chr = puzzle[rowIndex][i];
            char forwardChar = forward[idxOfChars];
            char reverseChar = reverse[idxOfChars];
            if (forwardChar != chr)
                fwdStillMatching = false;
            if (reverseChar != chr)
                reverseStillMatching = false;

            if ((!fwdStillMatching) && (!reverseStillMatching))
                return null;
            idxOfChars++;
        }
        if (fwdStillMatching)
            return new int[] {rowIndex, columnIndex, rowIndex, endPointer};
        else // Means reverse found
            return new int[] {rowIndex, endPointer, rowIndex, columnIndex};
    }

    /**
     * Searches vertically (both forwards and backwards
     * @param rowIndex index of row of the character
     * @param columnIndex index of column of the character
     * @param forward char array of the word to test for matching
     * @param reverse reversed char array of the word to test for matching - reverse done once and passed in
     *                to prevent reversing order in each cell;
     * @return null if not matched; int at index:
     *   0 = row index of the first letter;
     *   1 = column index of the first letter;
     *   2 = row index of the last letter;
     *   3 = column index of the last letter;
     */
    private int[] vertical(int rowIndex, int columnIndex, char[] forward, char[] reverse) {
        int endPointer = rowIndex + (forward.length - 1);
        if (endPointer >= rows) // Not Enough rows remain in column to match word
            return null;

        int idxOfChars = 0;
        boolean fwdStillMatching = true;
        boolean reverseStillMatching = true;
        for (int i = rowIndex; i < endPointer; i++) {
            char chr = puzzle[i][columnIndex];
            char forwardChar = forward[idxOfChars];
            char reverseChar = reverse[idxOfChars];
            if (forwardChar != chr)
                fwdStillMatching = false;
            if (reverseChar != chr)
                reverseStillMatching = false;

            if ((!fwdStillMatching) && (!reverseStillMatching))
                return null;
            idxOfChars++;
        }
        if (fwdStillMatching)
            return new int[] {rowIndex, columnIndex, endPointer, columnIndex};
        else // Means reverse found
            return new int[] {endPointer, columnIndex, rowIndex, columnIndex};
    }

    /**
     * Searches diagonally (both forwards and backwards
     * @param rowIndex index of row of the character
     * @param columnIndex index of column of the character
     * @param forward char array of the word to test for matching
     * @param reverse reversed char array of the word to test for matching - reverse done once and passed in
     *                to prevent reversing order in each cell;
     * @return null if not matched; int at index:
     *   0 = row index of the first letter;
     *   1 = column index of the first letter;
     *   2 = row index of the last letter;
     *   3 = column index of the last letter;
     */
    private int[] diagonal(int rowIndex, int columnIndex, char[] forward, char[] reverse) {
        int endRowPointer = rowIndex + (forward.length - 1);
        int endColumnPointer = columnIndex + (forward.length - 1);
        if ((endRowPointer >= rows) || (endColumnPointer >= columns)) // Not Enough rows remain in column to match word
            return null;
        int idx  = 0;
        boolean fwdStillMatching = true;
        boolean reverseStillMatching = true;
        int col = columnIndex;
        for (int row = rowIndex; row < endRowPointer; row++) {
            char chr = puzzle[row][col];
            char forwardChar = forward[idx];
            char reverseChar = reverse[idx];
            if (forwardChar != chr)
                fwdStillMatching = false;
            if (reverseChar != chr)
                reverseStillMatching = false;

            if ((!fwdStillMatching) && (!reverseStillMatching))
                return null;
            idx++;
            col++;
        }
        if (fwdStillMatching)
            return new int[] {rowIndex, columnIndex, endRowPointer, endColumnPointer};
        else // Means reverse found
            return new int[] {endRowPointer, endColumnPointer, rowIndex, columnIndex};
    }
}
